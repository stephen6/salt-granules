# encoding: utf-8
# vim: ft=python
'''
the primary goal for this pillar is to propigate the parsed data
from id_info.explode()
'''

import logging

log = logging.getLogger(__name__)


def ext_pillar(minion_id, pillar):  # pylint: disable=unused-argument
    '''
    this wee external pillar drops a minion's component-level data into
    the minion's pillar.
    '''

    retval = {}

    log.debug(' ext_pillar id_info, minion_id: %s', (minion_id,))
    boom = __salt__['id_info.explode'](minion_id)

    retval.update(boom.id_info)
    retval['id_info'] = boom.id_info

    return retval
