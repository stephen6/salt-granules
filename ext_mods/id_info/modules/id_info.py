# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name, too-many-branches, missing-docstring
'''
Because doing this in jinja is annoying
'''

import re
import logging
log = logging.getLogger(__name__)

__virtualname__ = 'id_info'


def __virtual__():
    return __virtualname__


def explode(id_):
    '''
    return a dict with the given (minion) id split up:

    example: ep-01.front.thing.001.lab.dfw.rackspace
        subcomponent: ep (endpoint)
        instance: (int)
        component: front(-end)
        project: thing
        cluster_id: numeric (still a string)
        environment: lab
        location: dc location
        provider: rackspace

        e.g.:
            lb-01.haproxy.someservice.001.prod.ec2.us-west-2
            lb-02.haproxy.someservice.001.prod.ec2.us-west-2
            routeapi-01.nginx.someservice.001.prod.ec2.us-west-2
            etc
    '''
    log.trace(' ext_mod id_info.explode: minion_id = %s', (id_,))

    bits = id_.split('.')

    number_host_bits = len(bits)

    class id_information(object):

        def __init__(self, id_):
            super(id_information, self).__init__()
            self.id_info = {}

            bits = id_.split('.')

            try:
                if number_host_bits > 0:
                    self.id_info.update({
                        'subcomponent': re.sub(r'-\d+$', '', bits[0])
                        })
                    self.id_info.update({
                        'instance': re.sub(r'^.*-(\d+)$', r'\1', bits[0])
                        })
                else:
                    self.id_info.update({'subcomponent': 'unknown'})

                if number_host_bits > 1:
                    self.id_info.update({'component': bits[1]})
                else:
                    self.id_info.update({'component': 'unknown'})

                if number_host_bits > 2:
                    self.id_info.update({'project': bits[2]})
                else:
                    self.id_info.update({'project': 'unknown'})


                if number_host_bits > 3:
                    self.id_info.update({'cluster_id': str(bits[3])})
                else:
                    self.id_info.update({'cluster_id': 'unknown'})

                if number_host_bits > 4:
                    self.id_info.update({'environment': bits[4]})
                else:
                    self.id_info.update({'environment': 'unknown'})

                if number_host_bits > 6:
                    self.id_info.update({'location': bits[6]})
                else:
                    self.id_info.update({'location': 'unknown'})

                if number_host_bits > 5:
                    provider = re.sub(r'_master$', '', bits[5])
                    self.id_info.update({'provider': provider})
                else:
                    self.id_info.update({'provider': 'unknown'})
            except Exception as onos:                   # pylint: disable=broad-except
                log.error(
                    'caught unspecified exception in ext_mod ' +
                    'id_info.explode(): %s', (onos,))

        def __call__(self):
            return self.id_info

        @property
        def subcomponent(self):
            return self.id_info['subcomponent']

        @property
        def instance(self):
            return self.id_info['instance']

        @property
        def component(self):
            return self.id_info['component']

        @property
        def cluster_id(self):
            return self.id_info['cluster_id']

        @property
        def project(self):
            return self.id_info['project']

        @property
        def environment(self):
            return self.id_info['environment']

        @property
        def location(self):
            return self.id_info['location']

        @property
        def provider(self):
            return self.id_info['provider']

        def __iter__(self):
            key_present = [
                'subcomponent',
                'instance',
                'component',
                'project',
                'cluster_id',
                'environment',
                'location',
                'provider'
            ]

            for k in key_present:
                yield self.__dict__['id_info'].get(k)

            raise StopIteration()

        def __repr__(self):
            bits = [
                    'subcomponent={0}'.format(self.subcomponent),
                    'instance={0}'.format(self.instance),
                    'component={0}'.format(self.component),
                    'project={0}'.format(self.project),
                    'cluster_id={}'.format(self.cluster_id),
                    'environment={0}'.format(self.environment),
                    'location={0}'.format(self.location),
                    'provider={0}'.format(self.provider),
                    ]
            return '<{0} {1}>'.format(self.__class__.__name__, ', '.join(bits))

        def __str__(self):
            return id_

    return id_information(id_)
